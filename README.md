# vendor_dexopt_profiles

Collection of dexopt profiles for Google apps.<br>
These profiles are for the latest gapps in [vendor_google_gms](https://gitlab.com/someone5678/vendor_google_gms).

## Add more apps' profiles to extraction list

To add more apps' profiles, update `prof-files.txt`.<br>
It should be follow the format.

```
com.package.name|AppFolderName
```

## Update Profiles

To update profiles they need to be generated from your phone<br>
while it's connected to adb. From vendor/google/dexopt_profiles:

```sh
./update-profiles.sh
```
