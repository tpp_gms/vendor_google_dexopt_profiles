#!/bin/bash

CWD=$(pwd)

rm -Rf common
mkdir -p common
cd common

for rawString in `cat ../prof-files.txt`; do
    packageName=${rawString%\|*}
    fileName=${rawString#*\|}
    # Get current profile for package
    adb shell cmd package snapshot-profile $packageName
    if [ $? -eq 0 ]; then
        # Pull profile if generated
        adb pull /data/misc/profman/$packageName.prof
        # Change name of successfully pulled profile
        mv $packageName.prof $fileName.prof
    fi
done

cd $CWD
